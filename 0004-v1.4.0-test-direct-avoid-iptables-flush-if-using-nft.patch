From 6069c94b5033fc82d8f35f2068a61374559d22de Mon Sep 17 00:00:00 2001
From: Eric Garver <eric@garver.life>
Date: Mon, 30 Jan 2023 14:43:18 -0500
Subject: [PATCH 4/4] v1.4.0: test(direct): avoid iptables flush if using
 nftables backend

Coverage: #863
(cherry picked from commit dcd0dd3674ea8ef757a1b41f6b53717a45e821aa)
---
 src/tests/features/features.at                |   1 +
 .../features/iptables_no_flush_on_shutdown.at | 144 ++++++++++++++++++
 2 files changed, 145 insertions(+)
 create mode 100644 src/tests/features/iptables_no_flush_on_shutdown.at

diff --git a/src/tests/features/features.at b/src/tests/features/features.at
index 78fe78c483ad..f59baea1cd70 100644
--- a/src/tests/features/features.at
+++ b/src/tests/features/features.at
@@ -19,3 +19,4 @@ m4_include([features/zone_combine.at])
 m4_include([features/startup_failsafe.at])
 m4_include([features/ipset.at])
 m4_include([features/reset_defaults.at])
+m4_include([features/iptables_no_flush_on_shutdown.at])
diff --git a/src/tests/features/iptables_no_flush_on_shutdown.at b/src/tests/features/iptables_no_flush_on_shutdown.at
new file mode 100644
index 000000000000..fbd7c793375c
--- /dev/null
+++ b/src/tests/features/iptables_no_flush_on_shutdown.at
@@ -0,0 +1,144 @@
+m4_if(nftables, FIREWALL_BACKEND, [
+
+dnl If FirewallBackend=nftables, and there are no --direct rules, then we can
+dnl avoid flushing iptables on shutdown. We can also avoid a flush on startup
+dnl if there are no permanent direct rules. But we will have to flush on the
+dnl first direct rule added.
+FWD_START_TEST([avoid iptables flush if using nftables])
+AT_KEYWORDS(direct gh863)
+CHECK_IPTABLES()
+
+dnl no flush on reload if no direct rules
+NS_CHECK([$IPTABLES -t filter -N firewalld_testsuite])
+NS_CHECK([$IPTABLES -t filter -I firewalld_testsuite -j ACCEPT])
+IF_HOST_SUPPORTS_IP6TABLES([
+NS_CHECK([$IP6TABLES -t filter -N firewalld_testsuite])
+NS_CHECK([$IP6TABLES -t filter -I firewalld_testsuite -j ACCEPT])
+])
+NS_CHECK([$EBTABLES -t filter -N firewalld_testsuite])
+NS_CHECK([$EBTABLES -t filter -I firewalld_testsuite -j ACCEPT])
+IPTABLES_LIST_RULES_ALWAYS([filter], [firewalld_testsuite], 0, [dnl
+    ACCEPT 0 -- 0.0.0.0/0 0.0.0.0/0
+])
+IP6TABLES_LIST_RULES_ALWAYS([filter], [firewalld_testsuite], 0, [dnl
+    ACCEPT 0 -- ::/0 ::/0
+])
+EBTABLES_LIST_RULES([filter], [firewalld_testsuite], 0, [dnl
+    -j ACCEPT
+])
+FWD_RELOAD()
+IPTABLES_LIST_RULES_ALWAYS([filter], [firewalld_testsuite], 0, [dnl
+    ACCEPT 0 -- 0.0.0.0/0 0.0.0.0/0
+])
+IP6TABLES_LIST_RULES_ALWAYS([filter], [firewalld_testsuite], 0, [dnl
+    ACCEPT 0 -- ::/0 ::/0
+])
+EBTABLES_LIST_RULES([filter], [firewalld_testsuite], 0, [dnl
+    -j ACCEPT
+])
+
+dnl no flush on restart (or stop) if no direct rules
+FWD_RESTART()
+IPTABLES_LIST_RULES_ALWAYS([filter], [firewalld_testsuite], 0, [dnl
+    ACCEPT 0 -- 0.0.0.0/0 0.0.0.0/0
+])
+IP6TABLES_LIST_RULES_ALWAYS([filter], [firewalld_testsuite], 0, [dnl
+    ACCEPT 0 -- ::/0 ::/0
+])
+EBTABLES_LIST_RULES([filter], [firewalld_testsuite], 0, [dnl
+    -j ACCEPT
+])
+
+dnl the first runtime direct rule should trigger an iptables flush
+FWD_CHECK([--direct --add-rule ipv4 filter INPUT 1 -j ACCEPT], 0, [ignore])
+IPTABLES_LIST_RULES_ALWAYS([filter], [firewalld_testsuite], 1, [ignore], [ignore])
+IP6TABLES_LIST_RULES_ALWAYS([filter], [firewalld_testsuite], 1, [ignore], [ignore])
+EBTABLES_LIST_RULES([filter], [firewalld_testsuite], 1, [ignore], [ignore])
+IPTABLES_LIST_RULES_ALWAYS([filter], [INPUT], 0, [dnl
+    ACCEPT 0 -- 0.0.0.0/0 0.0.0.0/0
+])
+NS_CHECK([$IPTABLES -t filter -N firewalld_testsuite])
+NS_CHECK([$IPTABLES -t filter -I firewalld_testsuite -j ACCEPT])
+IF_HOST_SUPPORTS_IP6TABLES([
+NS_CHECK([$IP6TABLES -t filter -N firewalld_testsuite])
+NS_CHECK([$IP6TABLES -t filter -I firewalld_testsuite -j ACCEPT])
+])
+NS_CHECK([$EBTABLES -t filter -N firewalld_testsuite])
+NS_CHECK([$EBTABLES -t filter -I firewalld_testsuite -j ACCEPT])
+IPTABLES_LIST_RULES_ALWAYS([filter], [firewalld_testsuite], 0, [dnl
+    ACCEPT 0 -- 0.0.0.0/0 0.0.0.0/0
+])
+IP6TABLES_LIST_RULES_ALWAYS([filter], [firewalld_testsuite], 0, [dnl
+    ACCEPT 0 -- ::/0 ::/0
+])
+EBTABLES_LIST_RULES([filter], [firewalld_testsuite], 0, [dnl
+    -j ACCEPT
+])
+FWD_RELOAD()
+IPTABLES_LIST_RULES_ALWAYS([filter], [firewalld_testsuite], 1, [ignore], [ignore])
+IPTABLES_LIST_RULES_ALWAYS([filter], [INPUT], 0, [dnl
+])
+IP6TABLES_LIST_RULES_ALWAYS([filter], [firewalld_testsuite], 1, [ignore], [ignore])
+IP6TABLES_LIST_RULES_ALWAYS([filter], [INPUT], 0, [dnl
+])
+EBTABLES_LIST_RULES([filter], [firewalld_testsuite], 1, [ignore], [ignore])
+EBTABLES_LIST_RULES([filter], [INPUT], 0, [dnl
+])
+
+dnl permanent direct rules should trigger a flush at start
+FWD_CHECK([--permanent --direct --add-rule ipv4 filter INPUT 1 -j ACCEPT], 0, [ignore])
+NS_CHECK([$IPTABLES -t filter -N firewalld_testsuite])
+NS_CHECK([$IPTABLES -t filter -I firewalld_testsuite -j ACCEPT])
+IF_HOST_SUPPORTS_IP6TABLES([
+NS_CHECK([$IP6TABLES -t filter -N firewalld_testsuite])
+NS_CHECK([$IP6TABLES -t filter -I firewalld_testsuite -j ACCEPT])
+])
+NS_CHECK([$EBTABLES -t filter -N firewalld_testsuite])
+NS_CHECK([$EBTABLES -t filter -I firewalld_testsuite -j ACCEPT])
+FWD_RELOAD()
+IPTABLES_LIST_RULES_ALWAYS([filter], [firewalld_testsuite], 1, [ignore], [ignore])
+IPTABLES_LIST_RULES_ALWAYS([filter], [INPUT], 0, [dnl
+    ACCEPT 0 -- 0.0.0.0/0 0.0.0.0/0
+])
+IP6TABLES_LIST_RULES_ALWAYS([filter], [firewalld_testsuite], 1, [ignore], [ignore])
+IP6TABLES_LIST_RULES_ALWAYS([filter], [INPUT], 0, [dnl
+])
+EBTABLES_LIST_RULES([filter], [firewalld_testsuite], 1, [ignore], [ignore])
+EBTABLES_LIST_RULES([filter], [INPUT], 0, [dnl
+])
+
+FWD_CHECK([--permanent --direct --remove-rule ipv4 filter INPUT 1 -j ACCEPT], 0, [ignore])
+FWD_RELOAD()
+
+dnl adding a chain should trigger a flush
+NS_CHECK([$IPTABLES -t filter -N firewalld_testsuite])
+NS_CHECK([$IPTABLES -t filter -I firewalld_testsuite -j ACCEPT])
+IF_HOST_SUPPORTS_IP6TABLES([
+NS_CHECK([$IP6TABLES -t filter -N firewalld_testsuite])
+NS_CHECK([$IP6TABLES -t filter -I firewalld_testsuite -j ACCEPT])
+])
+NS_CHECK([$EBTABLES -t filter -N firewalld_testsuite])
+NS_CHECK([$EBTABLES -t filter -I firewalld_testsuite -j ACCEPT])
+FWD_CHECK([--direct --add-chain ipv4 filter firewalld_foobar], 0, [ignore])
+IPTABLES_LIST_RULES_ALWAYS([filter], [firewalld_testsuite], 1, [ignore], [ignore])
+IP6TABLES_LIST_RULES_ALWAYS([filter], [firewalld_testsuite], 1, [ignore], [ignore])
+EBTABLES_LIST_RULES([filter], [firewalld_testsuite], 1, [ignore], [ignore])
+FWD_RELOAD()
+
+dnl adding a chain should trigger a flush
+NS_CHECK([$IPTABLES -t filter -N firewalld_testsuite])
+NS_CHECK([$IPTABLES -t filter -I firewalld_testsuite -j ACCEPT])
+IF_HOST_SUPPORTS_IP6TABLES([
+NS_CHECK([$IP6TABLES -t filter -N firewalld_testsuite])
+NS_CHECK([$IP6TABLES -t filter -I firewalld_testsuite -j ACCEPT])
+])
+NS_CHECK([$EBTABLES -t filter -N firewalld_testsuite])
+NS_CHECK([$EBTABLES -t filter -I firewalld_testsuite -j ACCEPT])
+FWD_CHECK([--direct --add-passthrough ipv4 -t filter -I INPUT -j ACCEPT], 0, [ignore])
+IPTABLES_LIST_RULES_ALWAYS([filter], [firewalld_testsuite], 1, [ignore], [ignore])
+IP6TABLES_LIST_RULES_ALWAYS([filter], [firewalld_testsuite], 1, [ignore], [ignore])
+EBTABLES_LIST_RULES([filter], [firewalld_testsuite], 1, [ignore], [ignore])
+
+FWD_END_TEST()
+
+])
-- 
2.39.3

